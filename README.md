# Avaliação técnica - Programador

1. Faça git clone do projeto.

2. Caso não tenha o Baixe o Banco de dados PostgreSQL instalado, baixe pelo link: https://www.enterprisedb.com/thank-you-downloading-postgresql?anid=1257095.

3. Execute o arquivo scriptSQL.sql. 

4. Edite o arquivo Dao.java, a qual está no caminho src\br\com\dao, e insira o nome de usuario, senha e o nome do banco de dados a qual se chama simulador.

5. Execute o projeto pelo Servidor Apache TomCat.

