<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Simula��o de Taxas</title>
		
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	</head>
	<body>
			<div class="container principal-color">
				<div>
					<h1>Simula��o de Taxas</h1>
					
				</div>
				<form method="post" action="valida" onSubmit="return validaForm()">
					<div class="form-group col-form-label"> 
						<label for="concorrente">Concorrente</label>
						<select required class="form-control col-sm-4" id="concorrente" name="concorrente">
							<option selected>Selecione</option>
							<option value="Cielo">Cielo</option>
							<option value="GetNet">GetNet</option>
							<option value="PagSeguro">PagSeguro</option>
							<option value="Stone Mais">Stone Mais</option>	
						</select>
					</div>
					<div class="form-group">
						<label for="identificador" class="col-form-label">CPF/CNPJ</label>
						<input type="number" required maxlength="14" class="form-control col-sm-4" id="identificador" name="identificador">
					</div>
					
					<div class="form-group">
						<label for="telefone" class="col-form-label">Telefone</label>
							<input type="number" required maxlength="9" class="form-control col-sm-4" id="telefone" name="telefone">	
					</div>
					
					<div class="form-group">
						<label for="email" class="col-form-label">Email</label>
							<input type="email" class="form-control col-sm-4" id="email" name="email">
					</div>
					
					<div class="form-group">
						<label for="ramoAtividade" class="col-form-label">Ramo de Atividade</label>
						<select class="form-control col-sm-4" required id="ramoAtividade" name="ramoAtividade">
							<option selected>Selecione</option>
							<c:forEach items="${listaAtividades}" var="atividade">
								<option value="${atividade.getIdRamoAtividade()}">${atividade.ramoAtividade}</option>
							</c:forEach>
						</select>	
					</div>
					
					<div class="form-group">
						<label for="taxaDebito" class="col-form-label">Taxa D�bito Concorrente</label>
						<input type="number" required class="form-control col-sm-4" id="taxaDebito" name="taxaDebito">
					</div>
					
					<div class="form-group">
						<label for="descontoDebito" class="col-form-label">Desconto D�bito</label>
						<input type="number" required class="form-control col-sm-4" id="descontoDebito" name="descontoDebito">
					</div>		
			
					<div class="form-group">
						<label for="taxaCredito" class="col-form-label">Taxa Cr�dito Concorrente</label>
						<input type="number" required class="form-control col-sm-4" id="taxaCredito" name="taxaCredito">
					</div>	
			
					<div class="form-group">
						<label for="descontoCredito" class="col-form-label">Desconto Cr�dito</label>
						<input type="number" required class="form-control col-sm-4" id="descontoCredito" name="descontoCredito">
					</div>	
							
					<div class="form-group">
						<button type="submit" class="btn btn-primary col-sm-1">Enviar</button>		
					</div>
				</form>
			</div>		
		
		<c:if test="${fn:length(propNegada) > 0}">	
			<script>alert('${propNegada}')</script>
		</c:if>
		
		<c:if test="${fn:length(confirmarProposta) > 0}">
			<p>${confirmarProposta}</p>
		</c:if>
		
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	</body>
</html>

<script>

	function confirmarProposta() {
		let decisao = confirm('${propConfirmar}');
		if (decisao) {
			document.forms[0].action = "inserir/true";
			document.forms[0].submit();
		} else {
			document.forms[0].action = "inserir/false";
			document.forms[0].submit();
		}
	};
	
	function validaForm() {
		
		let selectConcorrente = document.getElementById("concorrente");
		let selectAtividade = document.getElementById("ramoAtividade");

		if (selectConcorrente.value == "Selecione") {
			alert("Selecione uma op��o para o campo Concorrente");
			return false;
		}
			
		if (selectAtividade.value == "Selecione") {
			alert("Selecione uma op��o para o campo Ramo de Atividade");
			return false;
		}
		return true;
	}
</script>