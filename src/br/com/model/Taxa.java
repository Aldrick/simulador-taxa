package br.com.model;

public class Taxa {
	
	private double taxaDebito;
	private double descontoDebito;
	private double taxaCredito;
	private double descontoCredito;
	private String ramoAtividade;
	private int idRamoAtividade;
	private boolean confirmacao;
	
	public boolean getConfirmacao() {
		return confirmacao;
	}

	public void setConfirmacao(boolean confirmacao) {
		this.confirmacao = confirmacao;
	}

	public String getRamoAtividade() {
		return ramoAtividade;
	}
	
	public void setRamoAtividade(String ramoAtividade) {
		this.ramoAtividade = ramoAtividade;
	}
	
	public int getIdRamoAtividade() {
		return idRamoAtividade;
	}
	
	public void setIdRamoAtividade(int idRamoAtividade) {
		this.idRamoAtividade = idRamoAtividade;
	}
	
	public double getTaxaDebito() {
		return taxaDebito;
	}
	
	public void setTaxaDebito(double taxaDebito) {
		this.taxaDebito = taxaDebito;
	}
	
	public double getDescontoDebito() {
		return descontoDebito;
	}
	
	public void setDescontoDebito(double descontoDebito) {
		this.descontoDebito = descontoDebito;
	}
	
	public double getTaxaCredito() {
		return taxaCredito;
	}
	
	public void setTaxaCredito(double taxaCredito) {
		this.taxaCredito = taxaCredito;
	}
	
	public double getDescontoCredito() {
		return descontoCredito;
	}
	
	public void setDescontoCredito(double descontoCredito) {
		this.descontoCredito = descontoCredito;
	}
	
	public double calculaCredito() {
		return getTaxaCredito() - getDescontoDebito();
	}
	
	public double calculaDebito() {
		return getTaxaDebito() - getDescontoDebito();
	}
	
	public boolean verificaTaxaCredito (double taxaCredito) {
		
		if (calculaCredito() < taxaCredito)
			return false;
		return true;
	}
	
	public boolean verificaTaxaDebito (double taxaDebito) {
		
		if (calculaDebito() < taxaDebito)
			return false;
		return true;
	}
}
