package br.com.model;

public class Dados {
	
	private String identificador;
	private String email;
	private String telefone;
	private String concorrente;

	public String getIdentificador() {
		return identificador;
	}
	
	public String getConcorrente() {
		return concorrente;
	}
	
	public void setConcorrente(String concorrente) {
		this.concorrente = concorrente;
	}
	
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
}
