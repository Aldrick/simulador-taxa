package br.com.dao;

import java.sql.Connection;
import java.sql.DriverManager;

public class Dao {
		
	public Connection getConexao() {
			Connection conexao = null;
			String usuario = "postgres", // Edite o Nome do usuario do Banco
				   senha = "", // Edite a Senha do Banco de Dados
				   nomeBancoDados = "simulador"; // Manter Nome do Banco			
			try {
				Class.forName("org.postgresql.Driver");
				conexao = DriverManager.getConnection("jdbc:postgresql://localhost:5432/" + nomeBancoDados,
						usuario, senha);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return conexao;
	}
}