package br.com.dao;

import br.com.model.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.ArrayList;

public class DadosDao extends Dao {
	
	public void inserir(Dados dados, Taxa taxa) {
		LocalDate date = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		try {
			Connection conexao = getConexao();
			String sql = "INSERT INTO DADOS_TAXA (CONCORRENTE, IDENTIFICADOR, TELEFONE, EMAIL,"
					+ " ID_RAMO_ATIVIDADE, TAXA_DEBITO, DESCONTO_DEBITO, TAXA_CREDITO, DESCONTO_CREDITO,"
					+ " TAXA_CREDITO_ACEITO, TAXA_DEBITO_ACEITO, DATA_ACEITE, CONFIRMACAO)"
					+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?::date,?)";
			PreparedStatement ps = conexao.prepareStatement(sql);
			ps.setString(1, dados.getConcorrente());
			ps.setString(2, dados.getIdentificador());
			ps.setString(3, dados.getTelefone());
			ps.setString(4, dados.getEmail());
			ps.setInt(5, taxa.getIdRamoAtividade());
			ps.setDouble(6, taxa.getTaxaDebito());
			ps.setDouble(7, taxa.getDescontoDebito());
			ps.setDouble(8, taxa.getTaxaCredito());
			ps.setDouble(9, taxa.getDescontoCredito());
			ps.setDouble(10, taxa.calculaCredito());
			ps.setDouble(11, taxa.calculaDebito());
			if (taxa.getConfirmacao() == true)
				ps.setString(12, date.format(formatter));
			else 
				ps.setString(12, null);
			ps.setBoolean(13, taxa.getConfirmacao());
			
			ps.execute();
			ps.close();
			conexao.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<Taxa> listarAtividade() {
		List<Taxa> listaAtividade = new ArrayList<>();
		try {
				Connection conexao = getConexao();
				Statement st = conexao.createStatement();
				String sql = "SELECT * FROM TAXA_MINIMA";
				ResultSet rs = st.executeQuery(sql);
				while (rs.next()) {
					Taxa taxa = new Taxa();
					taxa.setIdRamoAtividade(rs.getInt("ID"));
					taxa.setRamoAtividade(rs.getString("RAMO_ATIVIDADE"));
					taxa.setTaxaCredito(rs.getDouble("MIN_TAXA_CREDITO"));
					taxa.setTaxaDebito(rs.getDouble("MIN_TAXA_CREDITO"));
					listaAtividade.add(taxa);
			}
			st.close();
			conexao.close();
		} catch(Exception e) {
			e.printStackTrace();
		} 
		return listaAtividade;
	}
	
	public Taxa consultarTaxaMinima (Taxa taxaMinima, int idRamoAtividade) {
		try {
				Connection conexao = getConexao();
				String sql = "SELECT MIN_TAXA_CREDITO, MIN_TAXA_DEBITO FROM TAXA_MINIMA"
						+ " WHERE ID = ?";
				PreparedStatement ps = conexao.prepareStatement(sql);
				ps.setInt(1, idRamoAtividade);
				ResultSet rs = ps.executeQuery();
				if(rs.next()) {
					taxaMinima.setTaxaCredito(rs.getDouble("MIN_TAXA_CREDITO"));
					taxaMinima.setTaxaDebito(rs.getDouble("MIN_TAXA_DEBITO"));
				}
				ps.close();
				conexao.close();
		} catch (Exception e){
			e.printStackTrace();
		}
		return taxaMinima;
	}
}