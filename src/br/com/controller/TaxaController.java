package br.com.controller;
import br.com.model.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.dao.DadosDao;

@WebServlet("/")
public class TaxaController extends HttpServlet{
	private static final long serialVersionUID = 1L;
	
	private DadosDao dao;
	private Dados dados;
	private Taxa taxa;
	
	public void init() {
		this.dao = new DadosDao();
		this.dados = new Dados();
		this.taxa = new Taxa();
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		    throws ServletException, IOException {
		        doGet(request, response);
	}	
	
	protected void doGet(HttpServletRequest request, 
			HttpServletResponse response ) throws ServletException, IOException {
		String acao = request.getServletPath();
		try {
			switch (acao) {
				case "/valida":
					validarForm(request, response);
					break;
				case "/inserir/true":
					inserirDado(request, response, true);
					break;
				case "/inserir/false":
					inserirDado(request, response, false);
					break;
				default:
					showForm(request, response);
					break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	protected void showForm(HttpServletRequest request, 
			HttpServletResponse response ) throws ServletException, IOException {
		
		DadosDao dao = new DadosDao();
		List<Taxa> listaAtividades = dao.listarAtividade();
		request.setAttribute("listaAtividades", listaAtividades);
		request.getRequestDispatcher("simulacao.jsp").forward(request, response);
	}
	
	protected void validarForm(HttpServletRequest request, 
			HttpServletResponse response ) throws ServletException, IOException {
		
		
		dados.setConcorrente(request.getParameter("concorrente"));
		dados.setIdentificador(request.getParameter("identificador"));
		dados.setTelefone(request.getParameter("telefone"));
		dados.setEmail(request.getParameter("email"));
					
		String taxaDebito = request.getParameter("taxaDebito").replaceAll(",", "."),
			   descontoDebito = request.getParameter("descontoDebito").replaceAll(",", "."),
			   taxaCredito = request.getParameter("taxaCredito").replaceAll(",", "."),
			   descontoCredito = request.getParameter("descontoCredito").replaceAll(",", "."),
			   idRamoAtividade = request.getParameter("ramoAtividade");
		
		taxa.setTaxaDebito(Double.parseDouble(taxaDebito));
		taxa.setDescontoDebito(Double.parseDouble(descontoDebito));
		taxa.setTaxaCredito(Double.parseDouble(taxaCredito));
		taxa.setDescontoCredito(Double.parseDouble(descontoCredito));
		taxa.setIdRamoAtividade(Integer.parseInt(idRamoAtividade));
		
		Taxa taxaMinima = new Taxa(); 
		DadosDao dao = new DadosDao();
		taxaMinima = dao.consultarTaxaMinima(taxaMinima, taxa.getIdRamoAtividade());
		
		boolean validaTaxaCredito = taxa.verificaTaxaCredito(taxaMinima.getTaxaCredito());
		boolean validaTaxaDebito  = taxa.verificaTaxaDebito(taxaMinima.getTaxaDebito());
		
		String msg = "", destino = "";
		
		if (validaTaxaCredito && validaTaxaDebito) {			
			request.setAttribute("confirmarProposta", "<body onload='confirmarProposta()'></body>");
			msg = "Deseja confirmar a proposta ?";
			destino = "propConfirmar";
			
		} else {
			
			msg = "Proposta n�o permitida\\n\\n";			
			
			if(!validaTaxaCredito) 
				msg += "Taxa de Cr�dito abaixo do m�nimo\\n"
				+ "Taxa Minima de Cr�dito permitida : "+ taxaMinima.getTaxaCredito() + "\\n\\n";
			
			if(!validaTaxaDebito)
				msg += "Taxa de D�bito abaixo do m�nimo\\n"
				+ "Taxa M�nima de D�bito permitido : " + taxaMinima.getTaxaDebito() + " \\n";
 			destino = "propNegada";
		}
		
		request.setAttribute(destino, msg);
		showForm(request, response);
	}
	
	protected void inserirDado(HttpServletRequest request, 
			HttpServletResponse response, boolean confirmacao) throws ServletException, IOException {
		
		taxa.setConfirmacao(confirmacao);
		try {
			dao.inserir(dados, taxa);	
			
		} catch (Exception e) {
				e.printStackTrace();
		} finally {		
			ServletContext servletContext= getServletContext();
			response.sendRedirect(servletContext.getContextPath());
		}
	}
}